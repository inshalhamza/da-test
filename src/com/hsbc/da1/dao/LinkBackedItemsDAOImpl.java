package com.hsbc.da1.dao;

/**
 * In this Implementation we implemented ItemsDAO using list to store objects
 * @author inshal hamza
 */

import java.util.ArrayList;
import java.util.List;

import com.hsbc.da1.exception.ItemNotFoundException;
import com.hsbc.da1.model.Items;

public class LinkBackedItemsDAOImpl implements ItemsDAO {

	private List<Items> itemsList = new ArrayList<>();

	@Override
	public Items createItemEntry(Items Item) {
		itemsList.add(Item);
		return Item;
	}

	@Override
	public Items updateItemDetails(long itemCode, Items item) {
		for (Items curritem : itemsList) {
			if (curritem.getItemCode() == itemCode) {
				curritem = item;
			}
		}
		return item;

	}

	@Override
	public void deleteItem(long itemCode) {
		for (Items curritem : itemsList) {
			if (curritem.getItemCode() == itemCode) {
				curritem = null;
			}
		}

	}

	@Override
	public List<Items> fetchAllItems() {
		return itemsList;
	}

	@Override
	public Items fetchItemByID(long itemCode) throws ItemNotFoundException {
		for (Items curritem : itemsList) {
			if (curritem.getItemCode() == itemCode) {
				return curritem;
			}
		}
		throw new ItemNotFoundException("Item Does not exist");
	}

}
