package com.hsbc.da1.dao;

/**
 * This is the Interface for decoupling the classes layers.
 * This interface will be implemented by different classes which will serve as data store;
 * @author inshal hamza
 */
		

import java.util.List;

import com.hsbc.da1.exception.ItemNotFoundException;
import com.hsbc.da1.model.Items;

public interface ItemsDAO {

		Items createItemEntry(Items Item);

		Items updateItemDetails(long itemCode, Items item);

		void deleteItem(long itemCode);

		List<Items> fetchAllItems();

		Items fetchItemByID(long itemCode)throws ItemNotFoundException;
	}


