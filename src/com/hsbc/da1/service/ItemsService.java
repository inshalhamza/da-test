package com.hsbc.da1.service;

import java.util.List;

import com.hsbc.da1.exception.ItemNotFoundException;
import com.hsbc.da1.model.Items;

public interface ItemsService {

	Items createElectronicItemEntry(long itemCode, String itemName, double unitPrice, long quantity, long warranty);

	Items createFoodItemEntry(long itemCode, String itemName, double unitPrice, long quantity, String dateOfManufacture,
			String dateOfExpiry, String isVegi);

	Items createApparelItemEntry(long itemCode, String itemName, double unitPrice, long quantity, String sizeOfApparel,
			String materailApplied);

	void deleteItem(long itemCode);

	public void editItemPrice(long itemCode, double price) throws ItemNotFoundException;

	public List<Items> topItems();

	public Items fetchItemsById(long itemCode) throws ItemNotFoundException;

}
