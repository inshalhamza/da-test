package com.hsbc.da1.service;

import java.util.List;

import com.hsbc.da1.dao.ItemsDAO;
import com.hsbc.da1.dao.LinkBackedItemsDAOImpl;
import com.hsbc.da1.exception.ItemNotFoundException;
import com.hsbc.da1.model.ApparelItems;
import com.hsbc.da1.model.ElectronicItems;
import com.hsbc.da1.model.FoodItems;
import com.hsbc.da1.model.Items;

public class ItemsServiceImpl implements ItemsService {

	private ItemsDAO itemdao = new LinkBackedItemsDAOImpl();

	@Override
	public Items createElectronicItemEntry(long itemCode, String itemName, double unitPrice, long quantity,
			long warranty) {
		Items item = new ElectronicItems(itemCode, itemName, unitPrice, quantity, warranty);
		Items itemCreated = this.itemdao.createItemEntry(item);
		return itemCreated;
	}

	public Items createApparelItemEntry(long itemCode, String itemName, double unitPrice, long quantity,
			String sizeOfApparel, String materailApplied) {
		Items item = new ApparelItems(itemCode, itemName, unitPrice, quantity, sizeOfApparel, materailApplied);
		Items itemCreated = this.itemdao.createItemEntry(item);
		return itemCreated;
	}

	@Override
	public Items createFoodItemEntry(long itemCode, String itemName, double unitPrice, long quantity,
			String dateOfManufacture, String dateOfExpiry, String isVegi) {
		Items item = new FoodItems(itemCode, itemName, unitPrice, quantity, dateOfManufacture, dateOfExpiry, isVegi);
		Items itemCreated = this.itemdao.createItemEntry(item);
		return itemCreated;
	}

	@Override
	public void deleteItem(long itemCode) {
		this.itemdao.deleteItem(itemCode);

	}

	@Override
	public void editItemPrice(long itemCode, double price) throws ItemNotFoundException {
		Items item = this.itemdao.fetchItemByID(itemCode);
		item.setUnitPrice(price);
		this.itemdao.updateItemDetails(itemCode, item);
	}

	@Override
	public List<Items> topItems() {
		List<Items> itemsList = this.itemdao.fetchAllItems();
		return itemsList;
	}

	@Override
	public Items fetchItemsById(long itemCode) throws ItemNotFoundException {
		Items item = this.itemdao.fetchItemByID(itemCode);
		return item;
	}

}
