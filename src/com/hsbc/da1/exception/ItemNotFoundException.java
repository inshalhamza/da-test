package com.hsbc.da1.exception;

/**
 * This Class extends Exception class then overrides the funtionality.
 * this class will be used in catching the exception in project.
 * @author inshal hamza
 * @version 1.0
 *
 */

public class ItemNotFoundException extends Exception{

	private static final long serialVersionUID = 1L;

	public ItemNotFoundException(String Message){
		super(Message);
	}
	
	@Override
	public String getMessage() {
		return  super.getMessage();
	}
}
