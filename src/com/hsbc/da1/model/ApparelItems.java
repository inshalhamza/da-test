package com.hsbc.da1.model;

/**
 * 
 * @author inshal hamza
 * @description this class defines structure for apparel-items subclass to
 *              Items;
 */

public class ApparelItems extends Items {

	private String sizeOfApparel;
	private String materialApplied; // only cotton and woolen should be provided

	public ApparelItems(long itemCode, String itemName, double unitPrice, long quantity, String sizeOfApparel,
			String materailApplicable) {
		super(itemCode, itemName, unitPrice, quantity);
		this.sizeOfApparel = sizeOfApparel;
		this.materialApplied = materailApplicable;

		// TODO Auto-generated constructor stub
	}

	public String getSizeOfApparel() {
		return sizeOfApparel;
	}

	public void setSizeOfApparel(String sizeOfApparel) {
		this.sizeOfApparel = sizeOfApparel;
	}

	public String getMaterialApplicable() {
		return materialApplied;
	}

	public void setMaterialApplicable(String materialApplicable) {
		this.materialApplied = materialApplicable;
	}

	@Override
	public String toString() {
		return "ApparelItems [sizeOfApparel=" + sizeOfApparel + ", materialApplicable=" + materialApplied
				+ ", itemCode=" + itemCode + ", itemName=" + itemName + ", unitPrice=" + unitPrice + ", quantity="
				+ quantity + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((materialApplied == null) ? 0 : materialApplied.hashCode());
		result = prime * result + ((sizeOfApparel == null) ? 0 : sizeOfApparel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApparelItems other = (ApparelItems) obj;
		if (materialApplied == null) {
			if (other.materialApplied != null)
				return false;
		} else if (!materialApplied.equals(other.materialApplied))
			return false;
		if (sizeOfApparel == null) {
			if (other.sizeOfApparel != null)
				return false;
		} else if (!sizeOfApparel.equals(other.sizeOfApparel))
			return false;
		return true;
	}

}
