package com.hsbc.da1.model;

/**
 * 
 * @author inshal
 * @description this class defines structure for fooditems subclass to Items;
 */

public class FoodItems extends Items {

	private static String dateOfManufacture = "12/2020";
	private static String dateOfExpiry = "24 month from Manufacture";
	private static String isVegitarian;

	public FoodItems(long itemCode, String itemName, double unitPrice, long quantity, String dateOfManufacture,
			String dateOfExpiry, String isVegi) {
		super(itemCode, itemName, unitPrice, quantity);
		this.dateOfExpiry = dateOfExpiry;
		this.dateOfManufacture = dateOfManufacture;
		this.dateOfManufacture = "12/2020";
		this.dateOfExpiry = "24 month from Manufacture";
		this.isVegitarian = isVegi;
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "FoodItems [dateOfManufacture=" + dateOfManufacture + ", dateOfExpiry=" + dateOfExpiry + ", itemCode="
				+ itemCode + ", itemName=" + itemName + ", unitPrice=" + unitPrice + ", quantity=" + quantity + "]";
	}

	public String getDateOfManufacture() {
		return dateOfManufacture;
	}

	public void setDateOfManufacture(String dateOfManufacture) {
		this.dateOfManufacture = dateOfManufacture;
	}

	public String getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(String dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dateOfExpiry == null) ? 0 : dateOfExpiry.hashCode());
		result = prime * result + ((dateOfManufacture == null) ? 0 : dateOfManufacture.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		FoodItems other = (FoodItems) obj;
		if (dateOfExpiry == null) {
			if (other.dateOfExpiry != null)
				return false;
		} else if (!dateOfExpiry.equals(other.dateOfExpiry))
			return false;
		if (dateOfManufacture == null) {
			if (other.dateOfManufacture != null)
				return false;
		} else if (!dateOfManufacture.equals(other.dateOfManufacture))
			return false;
		return true;
	}

}
