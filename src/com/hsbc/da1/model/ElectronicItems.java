package com.hsbc.da1.model;

/**
 * This class defines structure for electronicitems subclass to Items;
 *
 * @author inshal hamza
 * @version 1.0
 *
 */

public class ElectronicItems extends Items {

	private long warranty;

	public ElectronicItems(long itemCode, String itemName, double unitPrice, long quantity, long warranty) {
		super(itemCode, itemName, unitPrice, quantity);
		this.warranty = warranty;

	}

	public long getWarranty() {
		return warranty;
	}

	public void setWarranty(long warranty) {
		this.warranty = warranty;
	}

	@Override
	public String toString() {
		return "ElectronicItems [warranty=" + warranty + ", itemCode=" + itemCode + ", itemName=" + itemName
				+ ", unitPrice=" + unitPrice + ", quantity=" + quantity + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (int) (warranty ^ (warranty >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ElectronicItems other = (ElectronicItems) obj;
		if (warranty != other.warranty)
			return false;
		return true;
	}

}
