package com.hsbc.da1.client;

import java.util.List;

import com.hsbc.da1.exception.ItemNotFoundException;
import com.hsbc.da1.model.Items;
import com.hsbc.da1.service.ItemsService;
import com.hsbc.da1.service.ItemsServiceImpl;

/**
 * This class will be presented first where user will interact; version : 1.0
 * 
 */

public class ItemsClient {

	public static void main(String[] args) {

		ItemsService itemService = new ItemsServiceImpl();

		Items Tv = itemService.createElectronicItemEntry(1013, "TV", 3000, 100, 12);
		Items mobile = itemService.createElectronicItemEntry(1026, "Mobile", 8000, 24, 24);
		Items Watch = itemService.createElectronicItemEntry(1114, "Watch", 1500, 54, 18);

		Items milk = itemService.createFoodItemEntry(101, "Milk", 15, 40, "12/2020", "24 months", "Yes");
		Items curd = itemService.createFoodItemEntry(102, "curd", 20, 20, "9/2020", "6 months", "Yes");
		Items cake = itemService.createFoodItemEntry(101, "cake", 150, 12, "10/2020", "2 days", "Yes");

		Items tshirt = itemService.createApparelItemEntry(222, "T-shirt", 200, 50, "Large", "Cotton");
		Items shirt = itemService.createApparelItemEntry(432, "shirt", 300, 32, "Medium", "Cotton");
		Items sweater = itemService.createApparelItemEntry(325, "Sweater", 700, 20, "Medium", "Woolen");

		System.out.println();
		System.out.println("-----ElectronicItems-----");
		System.out.println("ItemCode: " + Tv.getItemCode() + " ItemName: " + Tv.getItemName() + " UnitPrice: "
				+ Tv.getUnitPrice() + " Quantity: " + Tv.getQuantity());
		System.out.println("ItemCode: " + mobile.getItemCode() + " ItemName: " + mobile.getItemName() + " UnitPrice: "
				+ mobile.getUnitPrice());
		System.out.println("ItemCode: " + Watch.getItemCode() + " ItemName: " + Watch.getItemName() + " UnitPrice: "
				+ Watch.getUnitPrice());

		System.out.println();
		System.out.println("-----FoodItems-----");
		System.out.println("ItemCode: " + milk.getItemCode() + " ItemName: " + milk.getItemName() + " UnitPrice: "
				+ milk.getUnitPrice() + " Quantity: " + milk.getQuantity());
		System.out.println("ItemCode: " + curd.getItemCode() + " ItemName: " + curd.getItemName() + " UnitPrice: "
				+ curd.getUnitPrice() + " Quantity: " + curd.getQuantity());
		System.out.println("ItemCode: " + cake.getItemCode() + " ItemName: " + cake.getItemName() + " UnitPrice: "
				+ cake.getUnitPrice() + " Quantity: " + cake.getQuantity());

		System.out.println();

		System.out.println("-----ApparelItems-----");
		System.out.println("ItemCode: " + tshirt.getItemCode() + " ItemName: " + tshirt.getItemName() + " UnitPrice: "
				+ tshirt.getUnitPrice() + " Quantity: " + tshirt.getQuantity());
		System.out.println("ItemCode: " + shirt.getItemCode() + " ItemName: " + shirt.getItemName() + " UnitPrice: "
				+ shirt.getUnitPrice() + " Quantity: " + shirt.getQuantity());
		System.out.println("ItemCode: " + sweater.getItemCode() + " ItemName: " + sweater.getItemName() + " UnitPrice: "
				+ sweater.getUnitPrice() + " Quantity: " + sweater.getQuantity());

		System.out.println("-----ALLItems-----");
		List<Items> items = itemService.topItems();
		for (Items item : items) {
			System.out.println("ItemCode: " + item.getItemCode() + " ItemName: " + item.getItemName() + " UnitPrice: "
					+ item.getUnitPrice() + " Quantity: " + Tv.getQuantity());

		}

		System.out.println();
		System.out.println("-----Some Oerations on Items-----");
		System.out.println("Trying to update price of Tv from 3000 to 4000");
		try {
			itemService.editItemPrice(Tv.getItemCode(), 4000);
			System.out.println("After update");
			System.out.println("ItemCode: " + Tv.getItemCode() + " ItemName: " + Tv.getItemName() + " UnitPrice: "
					+ Tv.getUnitPrice() + "Quantity: " + Tv.getQuantity());
		} catch (ItemNotFoundException i) {
			System.out.println(i.getMessage());
		}
	}
}
